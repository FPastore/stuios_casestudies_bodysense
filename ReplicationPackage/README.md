**Structure of the Package**
```
Package
   |---CaseStudy  //simple java implementation of the CarControl system along with 323 faulty versions.
   |---Config     //configuration files
   |---RQ1        //scripts to launch 44 faulty versions of the CarControl system
   |---RQ2        //scripts to launch 323 faulty versions of the CarControl system
   |---TestSuites //scripts to execute the different test suites
```

**Prerequisites**
1) Install R
2) Install "rJava", "binGroup", "binom" using R package installer
	With R 3.4 you may need to do the following:
	Within R:
		install.packages("rJava")
		install.packages("binGroup")
		install.packages("binom")
	In the console:
		sudo ln -f -s $(/usr/libexec/java_home)/jre/lib/server/libjvm.dylib /usr/local/lib
		
3) Set the variables R_HOME and R_JAVA in Config/rEnvConfig.sh according to you installation.

**Replicating RQ1 results**

1. Change the current working directory to 'ReplicationPackage'.
2. Launch the correct CarControl implementation ( './RQ1/CorrectVersion/launchSUT.sh' )
3. Start a new terminal (same working directory), select a test suite to execute. e.g. './TestSuites/executeReducedSTUIOSTestSuite.sh'
4. You will observe that all the test cases pass
--REPEAT STEPS 5 and 6 for all the 44 faulty versions 
5. Launch a faulty CarControl implementation, e.g. './RQ1/FaultySoftwareVersion1/launchSUT.sh'
6. Start a new terminal (same working directory), select a test suite to execute. e.g. './TestSuites/executeSTUIOSTestSuite0.sh'
	We suggest to redirect the output to a log file (e.g., ./TestSuites/executeSTUIOSTestSuite0.sh &> executeSTUIOSTestSuite0.log )
	Once the execution of the test suite finishes you will observe test failures being detected.
	All the test suites will fail (perfect precision and recall).


**Replicating RQ2 results**

1. Change the current working directory to 'ReplicationPackage'.
--REPEAT STEPS 2, 3 and 4 for all the 323 faulty versions
2. Launch a faulty CarControl implementation, e.g. './RQ1/FaultySoftwareVersion1/launchSUT.sh'
--REPEAT STEPS 3 and 4 for all the test suites
3. Start a new terminal (same working directory), select a test suite to execute. e.g. './TestSuites/executeSTUIOSTestSuite0.sh'
	We suggest to redirect the output to a log file (e.g., ./TestSuites/executeSTUIOSTestSuite0.sh &> executeSTUIOSTestSuite0.log )
	Once the execution of the test suite finishes you will observe test failures being detected.
4. Execute the corresponding TAUC test suite (e.g., ./TestSuites/executeTAUCTestSuite0.sh )
	Redirect the output as indicated above.
	Over all the executions, you will observe that TAUC identifies 7% faults less than STUIOS.
	
	
	
