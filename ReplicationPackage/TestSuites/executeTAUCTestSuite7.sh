#!/usr/bin/env bash

source Config/rEnvConfig.sh

java -Djava.library.path=.:${R_JAVA} -cp "../target/test-classes/:../target/classes/:../lib/*" org.junit.runner.JUnitCore testsuites.TestSuiteTAUC7
