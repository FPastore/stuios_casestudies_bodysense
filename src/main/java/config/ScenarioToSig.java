package config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chunhui.wang on 23/02/2017.
 */
public class ScenarioToSig {
    private static Map<String, String> sigMap = new HashMap<>();
    public static final String UNDETECT_DEFAULT = "UNDETECT|VOLTAGE_LOW_ERROR"
            + ",UNDETECT|VOLTAGE_HIGH_ERROR"
            + ",UNDETECT|RAM_ERROR"
            + ",UNDETECT|ROM_ERROR"
            + ",UNDETECT|NVM_CRC_ERROR"
            + ",UNDETECT|WATCHDOG_RESET_ERROR"
            + ",UNDETECT|DISCARD_ERROR"
            + ",UNDETECT|CALIBRATION_NOT_LEARNED_ERROR"
            + ",UNDETECT|BUILD_CHECK_NOT_RUN_ERROR"
            + ",UNDETECT|BUILD_CHECK_ERROR"
            + ",UNDETECT|NVM_ACCESS_ERROR"
            + ",UNDETECT|SEAT_FRAME_CONNECTION_ERROR"
            + ",UNDETECT|SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR"
            + ",UNDETECT|CABLE_SHIELD_ERROR"
            + ",UNDETECT|TEMPERATURE_SENSOR_ERROR"
            + ",UNDETECT|INTERNAL_MEASUREMENT_PATH_ERROR"
            + ",UNDETECT|MEASUREMENT_SENSE_PATH_ERROR"
            + ",UNDETECT|TEMPERATURE_RANGE_ERROR"
            + ",UNDETECT|TEMPERATURE_RANGE_LIMIT_ERROR"
            + ",UNDETECT|MEASUREMENT_RANGE_ERROR"
            + ",UNDETECT|CALIBRATION_DATA_ERROR"
            + ",UNDETECT|SHORT_TO_UBAT_GND_ERROR";

    static {
        sigMap.put("s[1]","DETECT|VOLTAGE_LOW_ERROR");
        sigMap.put("s[3]","DETECT|VOLTAGE_LOW_ERROR");
        sigMap.put("s[5]","DETECT|VOLTAGE_HIGH_ERROR");
        sigMap.put("s[7]","DETECT|VOLTAGE_HIGH_ERROR");
        sigMap.put("s[9]","DETECT|RAM_ERROR");
        sigMap.put("s[11]","DETECT|RAM_ERROR");
        sigMap.put("s[13]","DETECT|ROM_ERROR");
        sigMap.put("s[15]","DETECT|ROM_ERROR");
        sigMap.put("s[17]","DETECT|NVM_CRC_ERROR");
        sigMap.put("s[19]","DETECT|NVM_CRC_ERROR");
        sigMap.put("s[21]","DETECT|WATCHDOG_RESET_ERROR");
        sigMap.put("s[23]","DETECT|WATCHDOG_RESET_ERROR");
        sigMap.put("s[25]","DETECT|DISCARD_ERROR");
        sigMap.put("s[27]","DETECT|DISCARD_ERROR");
        sigMap.put("s[29]","DETECT|CALIBRATION_NOT_LEARNED_ERROR");
        sigMap.put("s[31]","DETECT|CALIBRATION_NOT_LEARNED_ERROR");
        sigMap.put("s[33]","DETECT|BUILD_CHECK_NOT_RUN_ERROR");
        sigMap.put("s[35]","DETECT|BUILD_CHECK_NOT_RUN_ERROR");
        sigMap.put("s[37]","DETECT|BUILD_CHECK_ERROR");
        sigMap.put("s[39]","DETECT|BUILD_CHECK_ERROR");
        sigMap.put("s[41]","DETECT|NVM_ACCESS_ERROR");
        sigMap.put("s[43]","DETECT|NVM_ACCESS_ERROR");
        sigMap.put("s[45]","DETECT|SEAT_FRAME_CONNECTION_ERROR");
        sigMap.put("s[47]","DETECT|SEAT_FRAME_CONNECTION_ERROR");
        sigMap.put("s[49]","DETECT|SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR");
        sigMap.put("s[51]","DETECT|SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR");
        sigMap.put("s[53]","DETECT|CABLE_SHIELD_ERROR");
        sigMap.put("s[55]","DETECT|CABLE_SHIELD_ERROR");
        sigMap.put("s[57]","DETECT|TEMPERATURE_SENSOR_ERROR");
        sigMap.put("s[59]","DETECT|TEMPERATURE_SENSOR_ERROR");
        sigMap.put("s[61]","DETECT|INTERNAL_MEASUREMENT_PATH_ERROR");
        sigMap.put("s[63]","DETECT|INTERNAL_MEASUREMENT_PATH_ERROR");
        sigMap.put("s[65]","DETECT|MEASUREMENT_SENSE_PATH_ERROR");
        sigMap.put("s[67]","DETECT|MEASUREMENT_SENSE_PATH_ERROR");
        sigMap.put("s[69]","DETECT|TEMPERATURE_RANGE_ERROR");
        sigMap.put("s[71]","DETECT|TEMPERATURE_RANGE_ERROR");
        sigMap.put("s[73]","DETECT|TEMPERATURE_RANGE_LIMIT_ERROR");
        sigMap.put("s[75]","DETECT|TEMPERATURE_RANGE_LIMIT_ERROR");
        sigMap.put("s[77]","DETECT|MEASUREMENT_RANGE_ERROR");
        sigMap.put("s[79]","DETECT|MEASUREMENT_RANGE_ERROR");
        sigMap.put("s[81]","DETECT|CALIBRATION_DATA_ERROR");
        sigMap.put("s[83]","DETECT|CALIBRATION_DATA_ERROR");
        sigMap.put("s[85]","DETECT|SHORT_TO_UBAT_GND_ERROR");
        sigMap.put("s[87]","DETECT|SHORT_TO_UBAT_GND_ERROR");
    }

    public static String getSig(String scenario) {
        return sigMap.get(scenario);
    }
}
