package client;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Created by chunhui.wang on 24/02/2017.
 */
public class TestDriverClient {
    final static Logger LOGGER = LoggerFactory.getLogger(TestDriverClient.class);
    private Communication comm = null;

    public TestDriverClient() {
        resetCommunication();
    }

    private void resetCommunication() {
        if(comm != null)
            comm.close();
        comm = new Communication();
    }

    public boolean executeTestCase(List<String> steps) throws InterruptedException {
        try {
            for (String step : steps) {
                LOGGER.info("Execute step: {}", step);
                if (isMsgStep(step))
                    comm.sendMessage(parseMsg(step));
                if (isWaitStep(step))
                    Thread.sleep(parseTime(step));
                if (isCheckStep(step) && !doCheck(step))
                    return false;
            }
        } finally {
            comm.sendMessage("Reset.");
            comm.sendMessage("Bye.");
            resetCommunication();
        }
        return true;
    }

    private String parseMsg(String step) {
        return step.split(":")[1];
    }

    private boolean doCheck(String step) {
        return Arrays.stream(step.split(";"))
                .map(check -> check.split("\\|"))
                .map(parts -> compare(comm.sendRequest(parts[0]), parts[1]))
                .noneMatch(b -> !b);
    }

    private boolean isCheckStep(String step) {
        return step.startsWith("CHECK");
    }

    private long parseTime(String step) {
        return Long.parseLong(step.split(":")[1]);
    }

    private boolean isWaitStep(String step) {
        return step.startsWith("WAIT");
    }

    private boolean isMsgStep(String step) {
        return step.startsWith("MSG");
    }

    private boolean compare(String actual, String expect) {
        LOGGER.info("actual:{}; expect:{}", actual, expect);
        Map<String, String> actualMap = toMap(actual);
        Map<String, String> expectMap = toMap(expect);
        return expectMap.entrySet().stream()
                .allMatch(e -> e.getValue().equals(actualMap.get(e.getKey())));
    }

    private Map<String, String> toMap(String s) {
        return Arrays.stream(s.split(","))
                .map(entry -> entry.split("="))
                .collect(Collectors.toMap(e -> e[0], e -> e[1]));
    }


    @Test
    public void TestRun() throws Exception {
        List<String> steps = Arrays.asList("MSG:DETECT|TEMPERATURE_RANGE_ERROR"
                ,"WAIT:4800"
                ,"CHECK:TEMPERATURE_RANGE_ERROR|isDetected=true,isQualified=true,qc=1"
                ,"MSG:UNDETECT|TEMPERATURE_RANGE_ERROR"
                ,"WAIT:8200"
                ,"CHECK:TEMPERATURE_RANGE_ERROR|isDetected=false,isQualified=false,qc=1");

        boolean actual = executeTestCase(steps);

        assertEquals(true, actual);
    }
}
