package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by chunhui.wang on 23/02/2017.
 */
public class Communication {
    final static Logger LOGGER = LoggerFactory.getLogger(Communication.class);
    private final String serverHostname = "127.0.0.1";

    private Socket echoSocket = null;
    private PrintWriter out = null;
    private BufferedReader in = null;



    public Communication() {
        try {
            echoSocket = new Socket(serverHostname, 10008);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                    + "the connection to: " + serverHostname);
            System.exit(1);
        }
    }

    public boolean sendMessage(String msg) {
        out.println(msg);
        try {
            return in.readLine().equals("ack:" + msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String sendRequest(String request) {
        out.println(request);
        String response = null;
        try {
            response = in.readLine();
            LOGGER.info("res:{}", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void close() {
        try {
            out.close();
            in.close();
            echoSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
