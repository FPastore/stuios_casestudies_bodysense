package testadaptor;

/**
 * @author chunhui.wang 21/11/2017
 */
public abstract class CarControlAdaptorSingleExe extends CarControlAdaptor{
    @Override
    public boolean isSingleExecution() {
        return true;
    }
}
