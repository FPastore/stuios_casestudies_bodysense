package testadaptor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by chunhui.wang on 6/13/2016.
 */
public class VerifyStep implements TestStep {
    private Map<String, Map<String, String>> variables;

    public VerifyStep(JsonObject object) {
        this.variables = new HashMap<>();
        addVariables(object);
    }

    public Map<String, Map<String, String>> getVariables() {
        return variables;
    }

    private void addVariables(JsonObject object) {
        String instanceName = object.get("instance").getAsString();
        JsonArray variables = object.get("variables").getAsJsonArray();
        if(!this.variables.containsKey(instanceName))
            this.variables.put(instanceName, new HashMap<>());
        for(JsonElement variable : variables) {
            String varName = variable.getAsJsonObject().get("name").getAsString();
            String varValue = variable.getAsJsonObject().get("value").getAsString();
            this.variables.get(instanceName).put(varName, varValue);
        }
    }

    @Override
    public String toTestScript() {
        return "CHECK:" + this.variables.entrySet().stream()
                .map(entry -> entry.getKey() + "|" + getStateName(entry.getValue()))
                .collect(Collectors.joining(";CHECK:"));
    }

    private String getStateName(Map<String, String> varMap) {
        return varMap.entrySet().stream()
                .map(entry -> mapToKey(entry.getKey()) + "=" + entry.getValue())
                .collect(Collectors.joining(","));
    }

    private String mapToKey(String key) {
        return key.matches("Detected|Qualified") ? "is" + key : key;
    }

    @Override
    public void merge(Object o) {
        variables.putAll(((VerifyStep) o).getVariables());
    }

    @Override
    public boolean canAdjacentMerge(Object o) {
        return o != null && o instanceof VerifyStep;
    }
}
