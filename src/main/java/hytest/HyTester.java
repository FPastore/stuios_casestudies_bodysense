package hytest;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chunhui.wang on 26/02/2017.
 */
public class HyTester {
    final static Logger LOGGER = LoggerFactory.getLogger(HyTester.class);
    final static int MAX_RUNS = 100;

    private static Rengine re = null;

    class Interval {
        private double upper;
        private double lower;

        public Interval(double lower, double upper) {
            if(lower > upper)
                throw new RuntimeException("Upper bound should greater than the lower bound!");
            this.upper = upper;
            this.lower = lower;
        }

        public double size() {
            return upper - lower;
        }

        public boolean isOverlapping(Interval interval) {
            return interval.size() + this.size() >= Math.max(this.upper, interval.upper) - Math.min(this.lower, interval.lower);
        }
    }

    public HyTester() {
        // new R-engine
        if(re == null) {
            re = new Rengine(new String[]{"--vanilla"}, false, null);
            if (!re.waitForR()) {
                System.err.println("Cannot load R");
            }
            re.eval("library(binGroup);");
            re.eval("library(binom);");
        }
    }

    public HyTest hyTest(int total, int positive, double probability) {
        return hyTest(total, positive, new Interval(probability - 0.5, probability + 0.5));
    }

    public HyTest hyTest(int total, int positive, Interval estimateCI) {

        String cmd = "binom.confint(" + positive + "," + total + ",conf.level=0.95,method=\"wilson\");";
        REXP eval = re.eval(cmd);
        double upper = eval.asVector().at("upper").asDouble();
        double lower = eval.asVector().at("lower").asDouble();
        double confidenceInterval =  upper - lower;

        //cmd = "binTest(" + total + "," + positive + "," + probability + ", alternative = \"less\", method = \"Score\");";
        //eval = re.eval(cmd);
        //double fail = eval.asVector().at("p.value").asDouble();

        //cmd = "binTest(" + total + "," + positive + "," + probability + ", alternative = \"greater\", method = \"Score\");";
        //eval = re.eval(cmd);
        //double pass = eval.asVector().at("p.value").asDouble();

        //LOGGER.info("Interval: {}，p-value(fail): {}, p-value(pass): {}", confidenceInterval, fail, pass);
        LOGGER.info("Interval: {}，lower: {}, upper: {}", confidenceInterval, lower, upper);

        if (confidenceInterval <= 0.10 || total == MAX_RUNS) {
            //if (fail < 0.05) return HyTest.REJECT;
            //if (pass < 0.05) return HyTest.ACCEPT;
            return calHyTestResult(new Interval(lower, upper), estimateCI);
        }

        return HyTest.NOT_DECIDED;
    }

    private HyTest calHyTestResult(Interval interval, Interval estimateCI) {
        if(interval.isOverlapping(estimateCI))
            return  HyTest.ACCEPT;
        else {
            LOGGER.info("True CI: [{},{}]; Estimate CI: [{},{}]", interval.lower, interval.upper, estimateCI.lower, estimateCI.upper);
            return HyTest.REJECT;
        }
    }

//    public HyTest hyTest(int total, int positive, double probability) {
//
//        String cmd = "binom.confint(" + positive + "," + total + ",conf.level=0.95,method=\"wilson\");";
//        REXP eval = re.eval(cmd);
//        double confidenceInterval = eval.asVector().at("upper").asDouble() - eval.asVector().at("lower").asDouble();
//        LOGGER.info("Interval: {}", confidenceInterval);
//        if(confidenceInterval <= 0.10) {
//            cmd = "binTest(" + total + "," + positive + "," + probability + ", alternative = \"less\", method = \"Score\");";
//            eval = re.eval(cmd);
//            double fail = eval.asVector().at("p.value").asDouble();
//
//            LOGGER.info("p-value(fail): {}", fail);
//
//            if (fail < 0.05) return HyTest.REJECT;
//
//            cmd = "binTest(" + total + "," + positive + "," + probability + ", alternative = \"greater\", method = \"Score\");";
//            eval = re.eval(cmd);
//            double pass = eval.asVector().at("p.value").asDouble();
//
//            LOGGER.info("p-value(fail): {}", pass);
//            if (pass < 0.05) return HyTest.ACCEPT;
//
//            System.out.println(pass);
//        }
//
//        return HyTest.NOT_DECIDED;
//    }

    public void end() {
        // done…
        re.end();
    }

    public static void main(String[] args) {
        HyTester hyTester = new HyTester();
        System.out.println(hyTester.hyTest(36, 32, 0.93));
        hyTester.end();
    }
}
