package stuios.testcase.tc3;
import testadaptor.CarControlAdaptor;
public class TC066 extends CarControlAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\":\"s[77]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":0"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":1700"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[80]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[79]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[80]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":9900"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[77]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":21200"
+ "}");


sendMessage("{"
+ "\"message\":\"AB_ECU_INIT_INFO\","
+ "\"parameters\":{\"InitIgnition_Signal\":\"IG_INIT_ERROR\"},"
+ "\"timestamp\":21200"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":29400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[28]\","
+ "\"timestamp\":29400"
+ "}");


verify("{"
+ "\"instance\":\"MEASUREMENT_RANGE_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":37600"
+ "}");



}
}
