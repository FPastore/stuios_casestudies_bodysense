package stuios.testcase.tc3;
import testadaptor.CarControlAdaptor;
public class TC001 extends CarControlAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\":\"s[49]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":0"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":1700"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[52]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[51]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[52]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":9900"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[49]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[50]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":19800"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":24600"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[25]\","
+ "\"timestamp\":24600"
+ "}");


verify("{"
+ "\"instance\":\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":29400"
+ "}");



}
}
