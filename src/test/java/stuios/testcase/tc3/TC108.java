package stuios.testcase.tc3;
import testadaptor.CarControlAdaptor;
public class TC108 extends CarControlAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\":\"s[57]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":0"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":1700"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[60]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[59]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[60]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":9900"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


sendMessage("{"
+ "\"message\":\"CONTROL_REQUEST\","
+ "\"parameters\":{\"Service_ID\":\"CTRL_SID_POLLING\"},"
+ "\"timestamp\":16400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[57]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


verify("{"
+ "\"instance\":\"TEMPERATURE_SENSOR_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":22900"
+ "}");



}
}
