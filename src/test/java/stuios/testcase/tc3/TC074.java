package stuios.testcase.tc3;
import testadaptor.CarControlAdaptor;
public class TC074 extends CarControlAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\":\"s[81]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":0"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":1700"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[84]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[83]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[84]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":9900"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[81]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":21200"
+ "}");


sendMessage("{"
+ "\"message\":\"AB_ECU_STATUS\","
+ "\"parameters\":{\"Ignition_Signal\":\"IGON_VALID\","
+ "\"VelocityStatus\":\"DRIVING\","
+ "\"BeltBuckledStatus\":\"BUCKLE_INIT_ERROR\","
+ "\"CrashEvent\":\"CRASH\"},"
+ "\"timestamp\":21200"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":29400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[28]\","
+ "\"timestamp\":29400"
+ "}");


verify("{"
+ "\"instance\":\"CALIBRATION_DATA_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":37600"
+ "}");



}
}
