package stuios.testcase.tc3;
import testadaptor.CarControlAdaptor;
public class TC037 extends CarControlAdaptor{
    @Override
    public void generatedTest() {

executeScenario("{"
+ "\"scenario\":\"s[1]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":0"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":1700"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[4]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":4800"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[3]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":6500"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[4]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":8200"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":9900"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[1]\","
+ "\"parameters\": ["
+ "{}"
+ "],"
+ "\"timestamp\":16400"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":18100"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":21200"
+ "}");


sendMessage("{"
+ "\"message\":\"AB_ECU_STATUS\","
+ "\"parameters\":{\"Ignition_Signal\":\"IGON_VALID\","
+ "\"VelocityStatus\":\"UNDEFINED\","
+ "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
+ "\"CrashEvent\":\"CRASH\"},"
+ "\"timestamp\":21200"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"true\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"true\"}"
+ "],"
+ "\"timestamp\":29400"
+ "}");


executeScenario("{"
+ "\"scenario\":\"s[28]\","
+ "\"timestamp\":29400"
+ "}");


verify("{"
+ "\"instance\":\"VOLTAGE_LOW_ERROR\","
+ "\"variables\": ["
+ "{\"name\":\"Detected\","
+ "\"value\":\"false\"},"
+ "{\"name\":\"Qualified\","
+ "\"value\":\"false\"}"
+ "],"
+ "\"timestamp\":37600"
+ "}");



}
}
