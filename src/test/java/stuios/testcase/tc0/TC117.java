package stuios.testcase.tc0;

import testadaptor.CarControlAdaptor;

public class TC117 extends CarControlAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\":\"s[61]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":0"
                + "}");


        verify("{"
                +
                "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":1700"
                + "}");


        verify("{"
                +
                "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[64]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        verify("{"
                +
                "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[63]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        verify("{"
                +
                "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[64]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        verify("{"
                +
                "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":9900"
                + "}");


        verify("{"
                + "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":16400"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal\":\"IGON_VALID\","
                + "\"VelocityStatus\":\"STANDING\","
                + "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
                + "\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\":16400"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[61]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":16400"
                + "}");


        verify("{"
                + "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":18100"
                + "}");


        verify("{"
                + "\"instance\":\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":22900"
                + "}");


    }
}
