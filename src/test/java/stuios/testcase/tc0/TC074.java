package stuios.testcase.tc0;

import testadaptor.CarControlAdaptor;

public class TC074 extends CarControlAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\":\"s[29]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":0"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":1700"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[32]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[31]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[32]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":9900"
                + "}");


        verify("{"
                +
                "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":16400"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[29]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":16400"
                + "}");


        verify("{"
                + "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":18100"
                + "}");


        verify("{"
                + "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":21200"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal\":\"IG_DEFVAL\"},"
                + "\"timestamp\":21200"
                + "}");


        verify("{"
                + "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":29400"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[28]\","
                + "\"timestamp\":29400"
                + "}");


        verify("{"
                + "\"instance\":\"CALIBRATION_NOT_LEARNED_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":37600"
                + "}");


    }
}
