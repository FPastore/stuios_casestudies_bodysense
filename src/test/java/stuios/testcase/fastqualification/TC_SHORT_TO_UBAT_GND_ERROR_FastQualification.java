package stuios.testcase.fastqualification;

import testadaptor.CarControlAdaptor;

public class TC_SHORT_TO_UBAT_GND_ERROR_FastQualification extends CarControlAdaptor {
    @Override
    public double getProbability() {
        return 0.5f;
    }

    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[85]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 2550"
                + "}");

    }
}