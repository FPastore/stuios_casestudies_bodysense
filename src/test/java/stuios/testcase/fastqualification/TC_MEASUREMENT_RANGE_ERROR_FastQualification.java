package stuios.testcase.fastqualification;

import testadaptor.CarControlAdaptor;

public class TC_MEASUREMENT_RANGE_ERROR_FastQualification extends CarControlAdaptor {
    @Override
    public double getProbability() {
        return 0.5f;
    }

    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[77]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 2550"
                + "}");

    }
}