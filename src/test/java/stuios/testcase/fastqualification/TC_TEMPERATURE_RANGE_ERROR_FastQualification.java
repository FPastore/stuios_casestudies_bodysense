package stuios.testcase.fastqualification;

import testadaptor.CarControlAdaptor;

public class TC_TEMPERATURE_RANGE_ERROR_FastQualification extends CarControlAdaptor {
    @Override
    public double getProbability() {
        return 0.5f;
    }

    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[69]\","
                + "\"parameters\": ["
                + "{\"name\":null,\"value\":null}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"TEMPERATURE_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 2550"
                + "}");

    }
}