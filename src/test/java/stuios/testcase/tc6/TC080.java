package stuios.testcase.tc6;

import testadaptor.CarControlAdaptor;

public class TC080 extends CarControlAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\":\"s[85]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":0"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":1700"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[88]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":13000"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal"
                + "\":\"IGON_VALID\"},"
                + "\"timestamp\":13000"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[85]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":13000"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":14700"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":17800"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal\":\"IGON_INVALID\"},"
                + "\"timestamp\":17800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[88]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":17800"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":26000"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[28]\","
                + "\"timestamp\":26000"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":34200"
                + "}");


    }
}
