package stuios.testcase.tc6;

import testadaptor.CarControlAdaptor;

public class TC118 extends CarControlAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\":\"s[85]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":0"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":1700"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[88]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[87]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[88]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":8200"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":9900"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":16400"
                + "}");


        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID\":\"CTRL_SID_CLEAR_DTC\"},"
                + "\"timestamp\":16400"
                + "}");


        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID\":\"CTRL_SID_POLLING\"},"
                + "\"timestamp\":16440"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":21240"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[25]\","
                + "\"timestamp\":21240"
                + "}");


        verify("{"
                + "\"instance\":\"SHORT_TO_UBAT_GND_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":26040"
                + "}");


    }
}
