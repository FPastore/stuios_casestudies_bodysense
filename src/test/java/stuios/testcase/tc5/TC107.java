package stuios.testcase.tc5;

import testadaptor.CarControlAdaptor;

public class TC107 extends CarControlAdaptor {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\":\"s[41]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":0"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":1700"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IGON_INVALID\","
                + "\"VelocityStatus\":\"STANDING\","
                + "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
                + "\"CrashEvent\":\"NO_CRASH\"},"
                + "\"timestamp\":4800"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[44]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":4800"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":6500"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":13000"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IGON_VALID\","
                + "\"VelocityStatus\":\"DRIVING\","
                + "\"BeltBuckledStatus\":\"BUCKLE_INIT_ERROR\","
                + "\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\":13000"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[41]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\":13000"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":14700"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":17800"
                + "}");


        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal\":\"IGON_VALID\"},"
                + "\"timestamp\":17800"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"true\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"true\"}"
                + "],"
                + "\"timestamp\":26000"
                + "}");


        executeScenario("{"
                + "\"scenario\":\"s[28]\","
                + "\"timestamp\":26000"
                + "}");


        verify("{"
                + "\"instance\":\"NVM_ACCESS_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\","
                + "\"value\":\"false\"},"
                + "{\"name\":\"Qualified\","
                + "\"value\":\"false\"}"
                + "],"
                + "\"timestamp\":34200"
                + "}");


    }
}
