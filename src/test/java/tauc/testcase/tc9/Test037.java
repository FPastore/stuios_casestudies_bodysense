package tauc.testcase.tc9;

import testadaptor.CarControlAdaptorSingleExe;

public class Test037 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[77]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IG_DEFVAL\","
                + "\"VelocityStatus\":\"DRIVING\","
                + "\"BeltBuckledStatus\":\"BUCKLE_INIT_ERROR"
                + "\",\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[80]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID"
                + "\":\"CTRL_SID_POLLING\"},"
                + "\"timestamp\": 13000"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[77]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_RANGE_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal\":\"IG_DEFVAL\",\"VelocityStatus\":\"UNDEFINED\",\"BeltBuckledStatus\":\"BUCKLED\",\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 17800"
                + "}");


    }
}