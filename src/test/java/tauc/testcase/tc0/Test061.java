package tauc.testcase.tc0;

import testadaptor.CarControlAdaptorSingleExe;

public class Test061 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[65]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[68]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[67]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[68]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 9900"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal\":\"IG_DEFVAL\"},"
                + "\"timestamp\": 16400"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[65]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        verify("{"
                + "\"instance\": \"MEASUREMENT_SENSE_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


    }
}