package tauc.testcase.tc0;

import testadaptor.CarControlAdaptorSingleExe;

public class Test084 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[49]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[52]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[51]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[52]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 9900"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[49]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 21200"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID\":\"CTRL_SID_CLEAR_DTC\"},"
                + "\"timestamp\": 21200"
                + "}");


    }
}