package tauc.testcase.tc2;

import testadaptor.CarControlAdaptorSingleExe;

public class Test120 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[9]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID"
                + "\":\"CTRL_SID_POLLING\"},"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[12]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IGON_INVALID\","
                + "\"VelocityStatus\":\"VELO_DEFVAL\","
                + "\"BeltBuckledStatus\":\"BUCKLED\","
                + "\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 13000"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[9]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        verify("{"
                + "\"instance\": \"RAM_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal\":\"IG_INIT_ERROR\",\"VelocityStatus\":\"DRIVING\",\"BeltBuckledStatus\":\"BUCKLED\",\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 17800"
                + "}");


    }
}