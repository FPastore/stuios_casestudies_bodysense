package tauc.testcase.tc7;

import testadaptor.CarControlAdaptorSingleExe;

public class Test078 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[49]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[52]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID"
                + "\":\"CTRL_SID_POLLING\"},"
                + "\"timestamp\": 13000"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[49]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"CONTROL_REQUEST\","
                + "\"parameters\":{\"Service_ID\":\"CTRL_SID_CLEAR_DTC\"},"
                + "\"timestamp\": 17800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[51]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


    }
}