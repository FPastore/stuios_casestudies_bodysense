package tauc.testcase.tc8;

import testadaptor.CarControlAdaptorSingleExe;

public class Test121 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[5]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[8]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[7]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[8]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 9900"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[5]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 21200"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal\":\"IG_INIT_ERROR\",\"VelocityStatus\":\"VELO_DEFVAL\",\"BeltBuckledStatus\":\"BUCKLE_INIT_ERROR\",\"CrashEvent\":\"NO_CRASH\"},"
                + "\"timestamp\": 21200"
                + "}");


    }
}