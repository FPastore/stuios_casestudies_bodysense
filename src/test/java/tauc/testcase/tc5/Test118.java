package tauc.testcase.tc5;

import testadaptor.CarControlAdaptorSingleExe;

public class Test118 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[61]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[64]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal"
                + "\":\"IGON_INVALID\"},"
                + "\"timestamp\": 13000"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[61]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        verify("{"
                + "\"instance\": \"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        verify("{"
                + "\"instance\": \"INTERNAL_MEASUREMENT_PATH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_INIT_INFO\","
                + "\"parameters\":{\"InitIgnition_Signal\":\"IGON_VALID\"},"
                + "\"timestamp\": 17800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[64]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 17800"
                + "}");


    }
}