package tauc.testcase.tc3;

import testadaptor.CarControlAdaptorSingleExe;

public class Test053 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[17]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[20]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[19]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[20]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 9900"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[17]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


        verify("{"
                + "\"instance\": \"NVM_CRC_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 21200"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IGON_INVALID\","
                + "\"VelocityStatus\":\"VELO_DEFVAL\","
                + "\"BeltBuckledStatus\":\"BUCKLE_INIT_ERROR"
                + "\",\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 21200"
                + "}");


    }
}