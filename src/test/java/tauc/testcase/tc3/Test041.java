package tauc.testcase.tc3;

import testadaptor.CarControlAdaptorSingleExe;

public class Test041 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[5]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[8]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IG_DEFVAL\","
                + "\"VelocityStatus\":\"VELO_DEFVAL\","
                + "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
                + "\"CrashEvent\":\"NO_CRASH\"},"
                + "\"timestamp\": 13000"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IGON_VALID\","
                + "\"VelocityStatus\":\"VELO_DEFVAL\","
                + "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
                + "\"CrashEvent\":\"CRASH\"},"
                + "\"timestamp\": 13040"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[5]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13040"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14740"
                + "}");


        verify("{"
                + "\"instance\": \"VOLTAGE_HIGH_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 17840"
                + "}");


//{type:"MESSAGE"}
        sendMessage("{"
                + "\"message\":\"AB_ECU_STATUS\","
                + "\"parameters\":{\"Ignition_Signal"
                + "\":\"IG_INIT_ERROR\","
                + "\"VelocityStatus\":\"UNDEFINED\","
                + "\"BeltBuckledStatus\":\"NOT_BUCKLED\","
                + "\"CrashEvent\":\"NO_CRASH\"},"
                + "\"timestamp\": 17840"
                + "}");


    }
}