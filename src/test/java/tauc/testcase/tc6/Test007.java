package tauc.testcase.tc6;

import testadaptor.CarControlAdaptorSingleExe;

public class Test007 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[45]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[48]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[47]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[48]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 8200"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 9900"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[45]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[46]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 18100"
                + "}");


        verify("{"
                + "\"instance\": \"SEAT_FRAME_CONNECTION_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 19800"
                + "}");


    }
}