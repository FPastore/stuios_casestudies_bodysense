package tauc.testcase.tc6;

import testadaptor.CarControlAdaptorSingleExe;

public class Test041 extends CarControlAdaptorSingleExe {
    @Override
    public void generatedTest() {

        executeScenario("{"
                + "\"scenario\": \"s[73]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 0"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 1700"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[76]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 4800"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":true}"
                + "],"
                + "\"timestamp\": 6500"
                + "}");


        verify("{"
                + "\"instance\": "
                + "\"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[73]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 13000"
                + "}");


        verify("{"
                + "\"instance\": \"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":true},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        executeScenario("{"
                + "\"scenario\": \"s[74]\","
                + "\"parameters\": ["
                + "{}"
                + "],"
                + "\"timestamp\": 14700"
                + "}");


        verify("{"
                + "\"instance\": \"TEMPERATURE_RANGE_LIMIT_ERROR\","
                + "\"variables\": ["
                + "{\"name\":\"Detected\", \"value\":false},"
                + "{\"name\":\"Qualified\",\"value\":false}"
                + "],"
                + "\"timestamp\": 16400"
                + "}");


    }
}