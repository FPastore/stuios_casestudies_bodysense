package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        stuios.testcase.tc6.TC000.class,
        stuios.testcase.tc6.TC001.class,
        stuios.testcase.tc6.TC002.class,
        stuios.testcase.tc6.TC003.class,
        stuios.testcase.tc6.TC004.class,
        stuios.testcase.tc6.TC005.class,
        stuios.testcase.tc6.TC006.class,
        stuios.testcase.tc6.TC007.class,
        stuios.testcase.tc6.TC008.class,
        stuios.testcase.tc6.TC009.class,
        stuios.testcase.tc6.TC010.class,
        stuios.testcase.tc6.TC011.class,
        stuios.testcase.tc6.TC012.class,
        stuios.testcase.tc6.TC013.class,
        stuios.testcase.tc6.TC014.class,
        stuios.testcase.tc6.TC015.class,
        stuios.testcase.tc6.TC016.class,
        stuios.testcase.tc6.TC017.class,
        stuios.testcase.tc6.TC018.class,
        stuios.testcase.tc6.TC019.class,
        stuios.testcase.tc6.TC020.class,
        stuios.testcase.tc6.TC021.class,
        stuios.testcase.tc6.TC022.class,
        stuios.testcase.tc6.TC023.class,
        stuios.testcase.tc6.TC024.class,
        stuios.testcase.tc6.TC025.class,
        stuios.testcase.tc6.TC026.class,
        stuios.testcase.tc6.TC027.class,
        stuios.testcase.tc6.TC028.class,
        stuios.testcase.tc6.TC029.class,
        stuios.testcase.tc6.TC030.class,
        stuios.testcase.tc6.TC031.class,
        stuios.testcase.tc6.TC032.class,
        stuios.testcase.tc6.TC033.class,
        stuios.testcase.tc6.TC034.class,
        stuios.testcase.tc6.TC035.class,
        stuios.testcase.tc6.TC036.class,
        stuios.testcase.tc6.TC037.class,
        stuios.testcase.tc6.TC038.class,
        stuios.testcase.tc6.TC039.class,
        stuios.testcase.tc6.TC040.class,
        stuios.testcase.tc6.TC041.class,
        stuios.testcase.tc6.TC042.class,
        stuios.testcase.tc6.TC043.class,
        stuios.testcase.tc6.TC044.class,
        stuios.testcase.tc6.TC045.class,
        stuios.testcase.tc6.TC046.class,
        stuios.testcase.tc6.TC047.class,
        stuios.testcase.tc6.TC048.class,
        stuios.testcase.tc6.TC049.class,
        stuios.testcase.tc6.TC050.class,
        stuios.testcase.tc6.TC051.class,
        stuios.testcase.tc6.TC052.class,
        stuios.testcase.tc6.TC053.class,
        stuios.testcase.tc6.TC054.class,
        stuios.testcase.tc6.TC055.class,
        stuios.testcase.tc6.TC056.class,
        stuios.testcase.tc6.TC057.class,
        stuios.testcase.tc6.TC058.class,
        stuios.testcase.tc6.TC059.class,
        stuios.testcase.tc6.TC060.class,
        stuios.testcase.tc6.TC061.class,
        stuios.testcase.tc6.TC062.class,
        stuios.testcase.tc6.TC063.class,
        stuios.testcase.tc6.TC064.class,
        stuios.testcase.tc6.TC065.class,
        stuios.testcase.tc6.TC066.class,
        stuios.testcase.tc6.TC067.class,
        stuios.testcase.tc6.TC068.class,
        stuios.testcase.tc6.TC069.class,
        stuios.testcase.tc6.TC070.class,
        stuios.testcase.tc6.TC071.class,
        stuios.testcase.tc6.TC072.class,
        stuios.testcase.tc6.TC073.class,
        stuios.testcase.tc6.TC074.class,
        stuios.testcase.tc6.TC075.class,
        stuios.testcase.tc6.TC076.class,
        stuios.testcase.tc6.TC077.class,
        stuios.testcase.tc6.TC078.class,
        stuios.testcase.tc6.TC079.class,
        stuios.testcase.tc6.TC080.class,
        stuios.testcase.tc6.TC081.class,
        stuios.testcase.tc6.TC082.class,
        stuios.testcase.tc6.TC083.class,
        stuios.testcase.tc6.TC084.class,
        stuios.testcase.tc6.TC085.class,
        stuios.testcase.tc6.TC086.class,
        stuios.testcase.tc6.TC087.class,
        stuios.testcase.tc6.TC088.class,
        stuios.testcase.tc6.TC089.class,
        stuios.testcase.tc6.TC090.class,
        stuios.testcase.tc6.TC091.class,
        stuios.testcase.tc6.TC092.class,
        stuios.testcase.tc6.TC093.class,
        stuios.testcase.tc6.TC094.class,
        stuios.testcase.tc6.TC095.class,
        stuios.testcase.tc6.TC096.class,
        stuios.testcase.tc6.TC097.class,
        stuios.testcase.tc6.TC098.class,
        stuios.testcase.tc6.TC099.class,
        stuios.testcase.tc6.TC100.class,
        stuios.testcase.tc6.TC101.class,
        stuios.testcase.tc6.TC102.class,
        stuios.testcase.tc6.TC103.class,
        stuios.testcase.tc6.TC104.class,
        stuios.testcase.tc6.TC105.class,
        stuios.testcase.tc6.TC106.class,
        stuios.testcase.tc6.TC107.class,
        stuios.testcase.tc6.TC108.class,
        stuios.testcase.tc6.TC109.class,
        stuios.testcase.tc6.TC110.class,
        stuios.testcase.tc6.TC111.class,
        stuios.testcase.tc6.TC112.class,
        stuios.testcase.tc6.TC113.class,
        stuios.testcase.tc6.TC114.class,
        stuios.testcase.tc6.TC115.class,
        stuios.testcase.tc6.TC116.class,
        stuios.testcase.tc6.TC117.class,
        stuios.testcase.tc6.TC118.class,
        stuios.testcase.tc6.TC119.class,
        stuios.testcase.tc6.TC120.class,
        stuios.testcase.tc6.TC121.class

})
public class TestSuiteSTUIOS6 {
}
