package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        stuios.testcase.fastqualification.TC_BUILD_CHECK_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_BUILD_CHECK_NOT_RUN_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_CABLE_SHIELD_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_CALIBRATION_DATA_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_CALIBRATION_NOT_LEARNED_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_DISCARD_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_INTERNAL_MEASUREMENT_PATH_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_MEASUREMENT_RANGE_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_MEASUREMENT_SENSE_PATH_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_NVM_ACCESS_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_NVM_CRC_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_RAM_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_ROM_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_SEAT_FRAME_CONNECTION_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_SEAT_HEATER_CIRCUIT_INTEGRITY_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_SHORT_TO_UBAT_GND_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_TEMPERATURE_RANGE_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_TEMPERATURE_RANGE_LIMIT_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_TEMPERATURE_SENSOR_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_VOLTAGE_HIGH_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_VOLTAGE_LOW_ERROR_FastQualification.class,
        stuios.testcase.fastqualification.TC_WATCHDOG_RESET_ERROR_FastQualification.class

})
public class TestSuiteSTUIOSReduced {
}
