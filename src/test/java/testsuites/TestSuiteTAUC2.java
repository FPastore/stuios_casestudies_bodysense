package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author chunhui.wang 04/08/2017
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        tauc.testcase.tc2.Test000.class,
        tauc.testcase.tc2.Test001.class,
        tauc.testcase.tc2.Test002.class,
        tauc.testcase.tc2.Test003.class,
        tauc.testcase.tc2.Test004.class,
        tauc.testcase.tc2.Test005.class,
        tauc.testcase.tc2.Test006.class,
        tauc.testcase.tc2.Test007.class,
        tauc.testcase.tc2.Test008.class,
        tauc.testcase.tc2.Test009.class,
        tauc.testcase.tc2.Test010.class,
        tauc.testcase.tc2.Test011.class,
        tauc.testcase.tc2.Test012.class,
        tauc.testcase.tc2.Test013.class,
        tauc.testcase.tc2.Test014.class,
        tauc.testcase.tc2.Test015.class,
        tauc.testcase.tc2.Test016.class,
        tauc.testcase.tc2.Test017.class,
        tauc.testcase.tc2.Test018.class,
        tauc.testcase.tc2.Test019.class,
        tauc.testcase.tc2.Test020.class,
        tauc.testcase.tc2.Test021.class,
        tauc.testcase.tc2.Test022.class,
        tauc.testcase.tc2.Test023.class,
        tauc.testcase.tc2.Test024.class,
        tauc.testcase.tc2.Test025.class,
        tauc.testcase.tc2.Test026.class,
        tauc.testcase.tc2.Test027.class,
        tauc.testcase.tc2.Test028.class,
        tauc.testcase.tc2.Test029.class,
        tauc.testcase.tc2.Test030.class,
        tauc.testcase.tc2.Test031.class,
        tauc.testcase.tc2.Test032.class,
        tauc.testcase.tc2.Test033.class,
        tauc.testcase.tc2.Test034.class,
        tauc.testcase.tc2.Test035.class,
        tauc.testcase.tc2.Test036.class,
        tauc.testcase.tc2.Test037.class,
        tauc.testcase.tc2.Test038.class,
        tauc.testcase.tc2.Test039.class,
        tauc.testcase.tc2.Test040.class,
        tauc.testcase.tc2.Test041.class,
        tauc.testcase.tc2.Test042.class,
        tauc.testcase.tc2.Test043.class,
        tauc.testcase.tc2.Test044.class,
        tauc.testcase.tc2.Test045.class,
        tauc.testcase.tc2.Test046.class,
        tauc.testcase.tc2.Test047.class,
        tauc.testcase.tc2.Test048.class,
        tauc.testcase.tc2.Test049.class,
        tauc.testcase.tc2.Test050.class,
        tauc.testcase.tc2.Test051.class,
        tauc.testcase.tc2.Test052.class,
        tauc.testcase.tc2.Test053.class,
        tauc.testcase.tc2.Test054.class,
        tauc.testcase.tc2.Test055.class,
        tauc.testcase.tc2.Test056.class,
        tauc.testcase.tc2.Test057.class,
        tauc.testcase.tc2.Test058.class,
        tauc.testcase.tc2.Test059.class,
        tauc.testcase.tc2.Test060.class,
        tauc.testcase.tc2.Test061.class,
        tauc.testcase.tc2.Test062.class,
        tauc.testcase.tc2.Test063.class,
        tauc.testcase.tc2.Test064.class,
        tauc.testcase.tc2.Test065.class,
        tauc.testcase.tc2.Test066.class,
        tauc.testcase.tc2.Test067.class,
        tauc.testcase.tc2.Test068.class,
        tauc.testcase.tc2.Test069.class,
        tauc.testcase.tc2.Test070.class,
        tauc.testcase.tc2.Test071.class,
        tauc.testcase.tc2.Test072.class,
        tauc.testcase.tc2.Test073.class,
        tauc.testcase.tc2.Test074.class,
        tauc.testcase.tc2.Test075.class,
        tauc.testcase.tc2.Test076.class,
        tauc.testcase.tc2.Test077.class,
        tauc.testcase.tc2.Test078.class,
        tauc.testcase.tc2.Test079.class,
        tauc.testcase.tc2.Test080.class,
        tauc.testcase.tc2.Test081.class,
        tauc.testcase.tc2.Test082.class,
        tauc.testcase.tc2.Test083.class,
        tauc.testcase.tc2.Test084.class,
        tauc.testcase.tc2.Test085.class,
        tauc.testcase.tc2.Test086.class,
        tauc.testcase.tc2.Test087.class,
        tauc.testcase.tc2.Test088.class,
        tauc.testcase.tc2.Test089.class,
        tauc.testcase.tc2.Test090.class,
        tauc.testcase.tc2.Test091.class,
        tauc.testcase.tc2.Test092.class,
        tauc.testcase.tc2.Test093.class,
        tauc.testcase.tc2.Test094.class,
        tauc.testcase.tc2.Test095.class,
        tauc.testcase.tc2.Test096.class,
        tauc.testcase.tc2.Test097.class,
        tauc.testcase.tc2.Test098.class,
        tauc.testcase.tc2.Test099.class,
        tauc.testcase.tc2.Test100.class,
        tauc.testcase.tc2.Test101.class,
        tauc.testcase.tc2.Test102.class,
        tauc.testcase.tc2.Test103.class,
        tauc.testcase.tc2.Test104.class,
        tauc.testcase.tc2.Test105.class,
        tauc.testcase.tc2.Test106.class,
        tauc.testcase.tc2.Test107.class,
        tauc.testcase.tc2.Test108.class,
        tauc.testcase.tc2.Test109.class,
        tauc.testcase.tc2.Test110.class,
        tauc.testcase.tc2.Test111.class,
        tauc.testcase.tc2.Test112.class,
        tauc.testcase.tc2.Test113.class,
        tauc.testcase.tc2.Test114.class,
        tauc.testcase.tc2.Test115.class,
        tauc.testcase.tc2.Test116.class,
        tauc.testcase.tc2.Test117.class,
        tauc.testcase.tc2.Test118.class,
        tauc.testcase.tc2.Test119.class,
        tauc.testcase.tc2.Test120.class,
        tauc.testcase.tc2.Test121.class

})
public class TestSuiteTAUC2 {
}
